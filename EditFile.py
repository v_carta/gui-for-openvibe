import xml.etree.ElementTree as ET
import json
from StartProgram import StartProgram
from Tkinter import *
import tkMessageBox

class EditFile:

    @staticmethod
    def editXml(answers, window):
        if (not answers):
            tkMessageBox.showerror("", "Odaberite pitanje i stisnite OK, za potvrdu pitanja stisnite Apply")

        else:
            br=0
            tree = ET.parse('C:/OpenVibe/openvibe-2.0/share/openvibe/scenarios/bci-examples/auditory-p300/aud2-p300-xdawn-1-acquisition.xml')
            root = tree.getroot()
            if (tree):
                for box in root.findall('./Boxes/Box'):
                    if(box.find("Name").text=="Sound Player"):
                        for setting in box.findall("./Settings/Setting"):
                            print setting
                            name = setting.find("Name").text
                            value = setting.find("Value").text
                            if(name=="File to play" and
                                value!="${Path_Root}abcic_experiment/wav_stimuli/da_ne_wavs/whitenoise_trimmed_norm.wav" and
                                value!="${Path_Root}abcic_experiment/wav_stimuli/da_ne_wavs/silence.wav"):

                                answer = "${Path_Root}abcic_experiment/wav_stimuli/da_ne_wavs/"
                                answer += answers[br]
                                setting.find("Value").text=answer
                                br+=1


            tree.write('C:/OpenVibe/openvibe-2.0/share/openvibe/scenarios/bci-examples/auditory-p300/aud2-p300-xdawn-1-acquisition.xml')
            StartProgram.startOpenVibe(window)

    @staticmethod
    def editJson(question, answers):
        with open("q&a.json", 'r+') as file:
            newEntry = {
                "question": question,
                "answers": answers
            }
            newJson=json.load(file)
            newJson.append(newEntry)


        with open("q&a.json", 'w') as file:
            try:
                json.dump(newJson, file, indent=4)
            except ValueError:
                print "Cant write data to file, check if JSON"

    @staticmethod
    def deleteFromJson(window, question):
        with open("q&a.json", 'r+') as file:
            data = json.load(file)
            print question
            for element in data:
                print element["question"]
                if (element["question"] == question):
                    print "if"
                    data.remove(element)
            print data

            with open("q&a.json", 'w') as file:
                try:
                    json.dump(data, file, indent=4)
                except ValueError:
                    print "Cant write data to file, check if JSON"

        window.root.destroy()
        window.makeGui()
