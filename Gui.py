# -*- coding: utf-8 -*-
from Tkinter import *
import tkFileDialog

from NewQuestions import NewQuestions
from EditFile import EditFile
import json
import ttk


class Gui:
    question = ''
    answers = []
    odg = ""
    root = None
    listaOdgovora = None
    applyB = None

    @classmethod
    def makeGui(self):
        # za odabrati pitanje
        self.root = Tk()


        self.listaOdgovora = Label(self.root, text=self.odg)
        self.root.title("BCI")
        self.root.minsize(450, 350)

        self.applyB = Button(self.root, text="Apply", command=lambda: EditFile.editXml(self.answers, self.root))

        self.applyB.grid(row=8, column=3, sticky=W, padx=5, pady=5)

        # questionB = Button(root, text="Browse", command=lambda: self.openFile(root, "q")) #browse button za pitanje

        #  style = ttk.Style()
        #  style.configure("BW.TLabel", foreground="black", background="white", justify=LEFT, padding=15, font='helvetica 10')

        #  label = ttk.Label(self.root, style="BW.TLabel", text="Odaberi pitanje:")
        # label.pack(anchor=W)
        label = Label(self.root, text="Odaberi pitanje:")
        label.grid(row=0, column=0, padx=5, pady=10)

        MODES = json.load(open('q&a.json'))

        v = StringVar()
        v.set("0")  # initialize
        br = 0
        r = 2
        for i in MODES:
            b = Radiobutton(self.root, text=i["question"], variable=v, value=br, indicatoron=0, height=2, width=15)
            b.grid(row=r, column=0, sticky=W, padx=5, pady=5)
            br += 1
            r = r + 1
            # print mode

        # , relief="flat",
        btn = Button(self.root, text='OK', command=lambda: self.ispisi(self.root, v, MODES))
        btn.grid(row=8, column=0, sticky=E, padx=5, pady=35)

        add = Button(self.root, text='Dodaj pitanje', command=lambda: self.novoPitanje())
        add.grid(row=8, column=1, sticky=W, padx=5, pady=5)

        add = Button(self.root, text='Obriši pitanje', command=lambda: EditFile.deleteFromJson(self, MODES[int(v.get())]["question"]))
        add.grid(row=8, column=2, sticky=W, padx=5, pady=5)

        self.listaOdgovora.grid(row=2, column=1, columnspan=3, rowspan=3, sticky=W + E + N + S, padx=5, pady=5)
        self.root.mainloop()  # ako ovo nemamo prozor se brzo otvori i zatvori

    @classmethod
    def novoPitanje(self):
        NewQuestions.addNew(self)

    @classmethod
    def ispisi(self, root, v, MODES):
        self.applyB["command"] = lambda: EditFile.editXml(self.answers, self.root)
        self.applyB.grid()
        x = v.get()

        self.odg = "Ponudeni odgovori:\n\n"

        for i in range(5):
            self.odg += str(i + 1)
            self.odg += ". "
            self.odg += MODES[int(x)]["answers"][i]
            self.odg += "\n"

        self.listaOdgovora["text"] = self.odg

        self.answers = MODES[int(x)]["answers"]

    @classmethod
    def openFile(self, root=None, type=None):
        if (root != None and type != None):
            file = tkFileDialog.askopenfile(parent=root, mode='rb', title='Choose a file')
            if (file != None):
                filePathLabel = Label(root, text=file.name)
                filePathLabel.grid()

                if (type == "q"):  # ako otvaramo pitanje dodaj dva buttona za odgovore
                    applyB = Button(root, text="Apply", command=lambda: EditFile.editXml(self.question, self.answers))  # button za potvrdu, ako kliknemo mijenja se xml
                    self.question = file.name
                    answer1B = Button(root, text="Browse", command=lambda: Gui.openFile(root, "a"))
                    answer1B.grid()

                    answer2B = Button(root, text="Browse", command=lambda: Gui.openFile(root, "a"))
                    answer2B.grid()

                    applyB.grid()

                else:  # ako otvaramo odgovor spremi ga u polje s odgovorima
                    self.answers.append(file.name)
