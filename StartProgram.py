import os
import time
from Tkinter import *
import pyautogui
import tkMessageBox


class StartProgram:

    @staticmethod
    def startOpenVibe(window):
        try:
            os.startfile('C:\OpenVibe\openvibe-2.0\openvibe-designer.cmd')

            time.sleep(3)
            pyautogui.moveTo(600, 600)
            pyautogui.click()

            pyautogui.hotkey('fn', 'f7')

            window.destroy()

        except Exception, e:
            tkMessageBox.showerror("", "Greska pri otvaranju OpenVibe-a.")

