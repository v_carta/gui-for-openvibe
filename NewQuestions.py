# -*- coding: utf-8 -*-
from Tkinter import *
import tkFileDialog
import tkMessageBox
from EditFile import EditFile

class NewQuestions:

    newRoot=""
    @staticmethod
    def addNew(window):
        NewQuestions.newRoot = Tk()
        NewQuestions.newRoot.lift()

        NewQuestions.newRoot.title("BCI")
        NewQuestions.newRoot.minsize(450,350)

        pitanje = Label(NewQuestions.newRoot, text="Upiši novo pitanje:")
        pitanje.grid(row=1, column=0, sticky=W, padx=5, pady=15)
        question = Entry(NewQuestions.newRoot)
        question.grid(row=1, column=1, ipadx=30)

        print question.get()

        odgovori = Label(NewQuestions.newRoot, text="Odaberi odgovore:")
        odgovori.grid(row=3, column=0, sticky=W, padx=5, pady=15)

        #PRVI ODGOVOR
        a1 = Entry(NewQuestions.newRoot, width=30)
        a1.grid(row=5, column=0, columnspan=2, sticky=W, padx=5)
        a1.insert(0, "Prvi odgovor...")
        a1.config(state=DISABLED)
        answer1 = Button(NewQuestions.newRoot, text="Browse", command=lambda:NewQuestions.openAnswer(a1, "1"))
        answer1.grid(row=5, column=1, padx=5, pady=5)
        #answer1.pack(side=LEFT,padx=10, pady=10, fill=X)

        #DRUGI ODGOVOR
        a2 = Entry(NewQuestions.newRoot, width=30)
        a2.grid(row=6, column=0, padx=5)
        a2.insert(0, "Drugi odgovor...")
        a2.config(state=DISABLED)
        answer2 = Button(NewQuestions.newRoot, text="Browse", command=lambda:NewQuestions.openAnswer(a2, "2"))
        answer2.grid(row=6, column=1, padx=5, pady=5)
       
        #TRECI ODGOVOR
        a3 = Entry(NewQuestions.newRoot, width=30)
        a3.grid(row=7, column=0, padx=5)
        a3.insert(0, "Treći odgovor...")
        a3.config(state=DISABLED)
        answer3 = Button(NewQuestions.newRoot, text="Browse", command=lambda:NewQuestions.openAnswer(a3, "3"))
        answer3.grid(row=7, column=1, padx=5, pady=5)

        #CETVRTI ODGOVOR
        a4 = Entry(NewQuestions.newRoot, width=30)
        a4.grid(row=8, column=0, padx=5)
        a4.insert(0, "Četvrti odgovor...")
        a4.config(state=DISABLED)
        answer4 = Button(NewQuestions.newRoot, text="Browse", command=lambda:NewQuestions.openAnswer(a4, "4"))
        answer4.grid(row=8, column=1, padx=5, pady=5)

        #PETI ODGOVOR
        a5 = Entry(NewQuestions.newRoot, width=30)
        a5.grid(row=9, column=0, padx=5)
        a5.insert(0, "Peti odgovor...")
        a5.config(state=DISABLED)
        answer5 = Button(NewQuestions.newRoot, text="Browse", command=lambda:NewQuestions.openAnswer(a5, "5"))
        answer5.grid(row=9, column=1, padx=5, pady=5)


        apply = Button(NewQuestions.newRoot, text="Apply", command=lambda:NewQuestions.apply(window, question.get(), NewQuestions.newRoot, a1, a2, a3, a4, a5))
        apply.grid(row=10, column=0, sticky=E, pady=10)

    listaOdg=[];
    prvi = False
    drugi = False
    treci = False
    cetvrti = False
    peti = False

    @staticmethod
    def openAnswer(index, check):
        file = tkFileDialog.askopenfile(mode='rb', title='Choose a file')
        NewQuestions.newRoot.lift()
        if file != None:

            filePath = file.name
            list = filePath.split('/')
            l = len(list) - 1
            answer = list[l]

            index.config(state=NORMAL)
            index.delete(0, 'end')
            index.insert(0, answer)
            index.config(state=DISABLED)


            if check=="1":
                NewQuestions.prvi=True
            if check=="2":
                NewQuestions.drugi=True
            if check=="3":
                NewQuestions.treci=True
            if check=="4":
                NewQuestions.cetvrti=True
            if check=="5":
                NewQuestions.peti=True



    @staticmethod
    def apply(window, pitanje, root, a1, a2, a3, a4, a5):
        print NewQuestions.prvi, NewQuestions.drugi, NewQuestions.treci, NewQuestions.cetvrti, NewQuestions.peti

        if ((NewQuestions.prvi == False) or (NewQuestions.drugi == False) or (NewQuestions.treci == False) or (NewQuestions.cetvrti == False) or (NewQuestions.peti == False)):
            tkMessageBox.showerror("", "Nisu odabrani svi odgovori.")
            NewQuestions.newRoot.lift()
        elif pitanje == "":
            tkMessageBox.showerror("", "Nije upisano pitanje.")
        else:
            NewQuestions.listaOdg.append(a1.get())
            NewQuestions.listaOdg.append(a2.get())
            NewQuestions.listaOdg.append(a3.get())
            NewQuestions.listaOdg.append(a4.get())
            NewQuestions.listaOdg.append(a5.get())

            print pitanje
            print NewQuestions.listaOdg
            EditFile.editJson(pitanje, NewQuestions.listaOdg)
            root.destroy()
            window.root.destroy()
            window.makeGui()

