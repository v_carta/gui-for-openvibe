from Tkinter import *
import Tkinter,tkFileDialog
from Gui import Gui


root = Tk() #blank window

questionFile = ''

def openQuestion():
	file = tkFileDialog.askopenfile(parent=root,mode='rb',title='Choose a file')
	if file != None:
		question.set(file.name)
		questionPath.pack()
		
		filePath = file.name
		list = filePath.split('/')
		l=len(list) - 1
		questionFile = list[l]
		answer1B.pack()
		answer2B.pack()

		
def openAnswer1():
	file = tkFileDialog.askopenfile(parent=root,mode='rb',title='Choose a file')
	if file != None:
		answer1.set(file.name)
		answer1Path.pack()
		
		filePath = file.name
		list = filePath.split('/')
		l=len(list) - 1
		answer1File = list[l]


def openAnswer2():
	file = tkFileDialog.askopenfile(parent=root, mode='rb', title='Choose a file')
	if file != None:
		answer2.set(file.name)
		answer2Path.pack()

		filePath = file.name
		list = filePath.split('/')
		l = len(list) - 1
		answer2File = list[l]

		
#za odabrati pitanje
questionB = Button(root, text = "Browse", command=openQuestion)

#ispis putanje odabranog pitanja
question = StringVar()
questionPath = Label(root, textvariable=question)


#za odabir 1. odgovora
answer1B = Button(root, text = "Browse", command=openAnswer1)

answer1 = StringVar()
answer1Path = Label(root, textvariable=answer1)

#za odabir 2. odgovora
answer2B = Button(root, text = "Browse", command=openAnswer2)

answer2 = StringVar()
answer2Path = Label(root, textvariable=answer2)

questionB.pack()

root.mainloop() #ako ovo nemamo prozor se brzo otvori i zatvori 



